import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import { AppProps, App } from './App';

describe('<App />', () => {

  let div: any;
  let props: AppProps;

  beforeEach(() => {
    div = document.createElement('div');
    props = {
        title: 'Test Title'
    }; 
  });

  it('[SMOKE - DEEP] renders without crashing', () => {
    ReactDOM.render(<App {...props} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('[SMOKE - SHALLOW] renders without crashing', () => {
    shallow(<App {...props} />);
  });

});

