import React from 'react';
import { 
  BrowserRouter as Router, 
  Route, Switch 
} from 'react-router-dom';
import styles from './App.module.scss';

export interface AppProps {
  title: string
}

export const App: React.FC<AppProps> = (props) => {
  return (
    <Router>
      <Switch>
        <Route path="/"
          render={() => (
            <section className={styles.app}>
              {props.title}
            </section>
          )} />
      </Switch>
    </Router>
  );
}

App.defaultProps = {
  title: 'Hello World'
};

export default App;
